(part "2N7002"
    (packageRef "SOT95P230X110-3N")
    (interface
        (port "1" (symbPinId 1) (portName "G") (portType INPUT))
        (port "2" (symbPinId 2) (portName "S") (portType GROUND))
        (port "3" (symbPinId 3) (portName "D") (portType POWER))
    )
    (partClass UNDEF)
    (useInSchema Y)
    (useInLayout Y)
    (inPartsList Y)
    (partType NORMAL)
    (placeRestriction FREE)
    (property "compKind" "221")
    (property "Manufacturer_Name" "Nexperia")
    (property "Manufacturer_Part_Number" "2N7002")
    (property "Mouser_Part_Number" "N/A")
    (property "Mouser_Price/Stock" "https://www.mouser.com/Search/Refine.aspx?Keyword=N%2FA")
    (property "Arrow_Part_Number" "")
    (property "Arrow_Price/Stock" "")
    (property "Description" "2N7002 N-channel MOSFET, 300 mA, 60 V, 3-Pin TO-236AB")
    (property "Datasheet_Link" "https://datasheet.datasheetarchive.com/originals/distributors/Datasheets-26/DSA-502170.pdf")
    (property "symbolName1" "2N7002")
)
