
## Health Monitor (PCB HAT)

## Description
Our HAT is designed as a medical device capable of sensing your body temperature when you place your finger on the analogue proximity sensor. A red LED will switch on when the persons body temperate exceeds the safe range.It can extrapolate user data such as body temperature and whether one is showing common symptoms of illness. This device can be used by the elderly or persons with co-morbidities whose health is constantly at risk and will notify relevant parties in the event of a decline in the person's health. Another use of this device would be that if this device became common enough, it would be able to better track the viral contamination such as the COVID-19 outbreak or even the common cold. Restaurants and shops could use this device instead of having a staff member measuring the temperature of the clients. The device would be placed somewhere easily accessible i.e. on a wall.  

## Installation
This repo can be pulled and pushed from by project team members using git push and git pull. Teaching staff clone this repo to view progress. This repo contains files that may need additional software to run such as CAD files.

## Usage
This git repo is used for version control and sharing of files between project team members and for teaching staff to keep track of progress. 

## Support
Contact Git Repo manager for help if there are any issues: LCKYAS002@myuct.ac.za

## Roadmap
There will consistently be new versions of the files in this repo whilst the project is in development. This is due to the fact that this project is in developmental stages.

## Contributing
Contributions will only be taken from project members of this team and the teaching staff of this course, for now. This can be done by using git push and git pull. 


## Authors and acknowledgment
Yashil Luckan;
Amal Patel;
Pooja Jugnarayan

## License
Creative Commons.

## Project status
Actively in progress.
